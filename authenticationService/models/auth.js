const { PrismaClient } = require('@prisma/client')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt');
const prisma = new PrismaClient()

const fields = {
    id: true,
    name: true,
    email: true,
    password: false
}

class Auth {
    constructor(){}

    async login(userCredentials){
        const exist = await prisma.user.findUnique({
            where: {
                email: userCredentials.email,
            }
        })
        if(!exist){
            return {
                autenticado: false,
                user: null
            }
        } else {
            const autenticado =  await bcrypt.compare(userCredentials.password,exist.password )
            if(autenticado){
                return {
                    autenticado: autenticado,
                    user: {
                        id: exist.id,
                        name: exist.name,
                        email: exist.email
                    }
                }
            } else {
                return {
                    autenticado: autenticado,
                    user: null
                }
            }
        }
    }
     
}

module.exports = new Auth()