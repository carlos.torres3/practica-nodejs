const express = require('express')
var cors = require('cors')
const app = express()
app.use(cors())
var cookieParser = require('cookie-parser');
var logger = require('morgan');

require('dotenv').config();

const HOST = process.env.HOST
const PORT = process.env.PORT
const SERVICE_NAME = process.env.SERVICE_NAME

var consul = require('consul')(
    {
        host: process.env.HOST,
        secure:false
    }
);

var authRouter = require('./routes/auth');
var userRouter = require('./routes/user');


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.get("/", (req, res) => {
    res.status(200).end("Authentication Service")
})

app.get('/health', (req, res) => {
    res.status(200).end( "OK." )
});


app.use('/auth', authRouter);
app.use('/user', userRouter);

var check = {
  
    "id": SERVICE_NAME+"-"+PORT,
    "name": "Servicio de Autenticacion",
    "address": HOST,
    "port": parseInt(PORT),
    "check": {
        "Http": "http://"+HOST+":"+PORT+"/health",
        "Interval": "10s",
        "Timeout": "10s",
        "DeregisterCriticalServiceAfter": "1m"
    } 
  };
consul.agent.service.register(check, ()=>{});

app.listen(PORT, () => {
    
})

module.exports = app;


