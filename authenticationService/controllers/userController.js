'use strict'

const UserService = require('../services/userService');

class UserController {
    constructor(){}

    async getById(req, res) {
        const result =  await UserService.getById(req.params.id)
        return res.status(result.statusCode).send(result)        
    }

    async getByEmail(req, res) {
        const result =  await UserService.getByEmail(req.body.email)
        return res.status(result.statusCode).send(result)        
    }

    async create(req,res){
        const result =  await UserService.create(req.body)
        return res.status(result.statusCode).send(result) 
    }
}

module.exports = new UserController()