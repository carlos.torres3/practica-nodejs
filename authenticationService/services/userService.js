
const jwt = require('jsonwebtoken')
const User = require('../models/user');
const { ApiResponse, ApiStatus } = require('../responses/apiResponse');

class UserService {
    
    constructor(){}

    async create(user){
        const result = await User.create(user)
        if(!result){
            return new ApiResponse(ApiStatus.ERROR, 400,"El email ya se encuentra resgistrado",null)
        } else {
            const payload = {
                check: true,
                exp: Math.floor(Date.now() / 1000) + (parseInt(process.env.JWT_TTL))
            }
            const token = jwt.sign(payload, process.env.JWT_SECRET)
            
            return new ApiResponse(ApiStatus.OK, 200,null,{ user: result, token}) 
        }
    }

    async getById(id) {
        const result  = await User.getById(id)
        if(result){
            return new ApiResponse(ApiStatus.OK, 200,null,result)
        } else {
            return new ApiResponse(ApiStatus.ERROR, 400,"Usuario no encontrado",null)
        }
    }

    async getByEmail(email) {
        const result  = await User.getByEmail(email)
        if(result){
            return new ApiResponse(ApiStatus.OK, 200,null,result)
        } else {
            return new ApiResponse(ApiStatus.ERROR, 400,"Usuario no encontrado",null)
        }
    }

}

module.exports = new UserService()