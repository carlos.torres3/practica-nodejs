
const jwt = require('jsonwebtoken')
const Auth = require('../models/auth');
const { ApiResponse, ApiStatus } = require('../responses/apiResponse');

class AuthService {
    
    constructor(){}

    async login(userCredentials){
        const autenticate = await Auth.login(userCredentials)
        const user = autenticate.user 
        if (autenticate.autenticado){
            const payload = {
                user: autenticate.user,
                exp: Math.floor(Date.now() / 1000) + (parseInt(process.env.JWT_TTL))
            }
            const token = jwt.sign(payload, process.env.JWT_SECRET)
            return new ApiResponse(ApiStatus.OK, 200,null,{ token, user }) 
        } else {
            return new ApiResponse(ApiStatus.ERROR, 401,"No podemos autenticar con la información suministrada",null)
        }
    }

    async verify(token){
        try {
            const valid = jwt.verify(token, process.env.JWT_SECRET)
            return new ApiResponse(ApiStatus.OK, 200,"El token es valido",valid)             
        } catch (error) {
            return new ApiResponse(ApiStatus.ERROR, 401,"El token no es valido o ya expiro",null)
        }
    }
}

module.exports = new AuthService();