const UserController  = require('../controllers/userController');
var express = require('express');
var router = express.Router();

router.get('/:id', async (req, res) => {
  return await UserController.getById(req, res)
});

router.post('/getByEmail', async (req, res) => {
  return await UserController.getByEmail(req, res)
});



module.exports = router;