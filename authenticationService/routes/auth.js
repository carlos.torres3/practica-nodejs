'use strict'

const AuthController  = require('../controllers/authController');
const UserController  = require('../controllers/userController');
var express = require('express');
var router = express.Router();

router.post('/login', async (req,res) => {
  return await AuthController.login(req,res)
});
/* istanbul ignore next */
router.post('/verify', async (req,res) => {
  return await AuthController.verify(req,res)
});

router.post('/register', async (req,res) => {
  return await UserController.create(req,res)
});

module.exports = router;
