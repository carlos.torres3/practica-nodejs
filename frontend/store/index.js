import { getBalance } from '../services/transaction'

export const state = () => ({
    user: {},
    balance: {},
  })
  
  export const getters = {}
  
  export const actions = {
    setUser({ commit }, payload) {
        commit('SET_USER', payload)
    },
    async setBalance({ commit }, payload) {
        commit('SET_BALANCE', payload)
    },
  }
  
  export const mutations = {
    SET_USER(state, payload) {
      state.user = payload
    },
    SET_BALANCE(state, payload) {
      state.balance = payload
    },
  }

  