import Vue from 'vue'
import { login } from '../../services/auth'
import { mapActions } from 'vuex'

export default Vue.extend({
    name:'Login',
    head: {
        bodyAttrs: {
          class: 'text-center'
        },
        meta: [
            { name: 'viewport', content: 'width=device-width, initial-scale=1' }
        ]
    },
    layout: 'auth/auth',
    data:() =>({
        credentials:{
            email:'',
            password:''
        },
        errors: []
    }),
    created(){
        
    },
    mounted() {

    },
    methods:{
        // async login(){
        //     this.errors = [];
        //     if (!this.credentials.email) {
        //         this.errors.push('El correo electrónico es requerido.');
        //     } else if (!this.validEmail(this.credentials.email)) {
        //         this.errors.push('El correo electrónico debe ser válido.');
        //     }
        //     if (!this.credentials.password) {
        //         this.errors.push("La contraseña es requerida");
        //     }
            
        //     if (this.errors.length === 0) {
        //         const auth = await login(this.credentials)
        //         if(auth){
        //             this.$router.push('/dashboard')
        //         } else {
        //             this.$swal.fire({
        //                 icon: 'error',
        //                 title: 'No Autorizado',
        //                 text: 'Sus credenciales son ivalidad'
        //             })
        //         }
        //     } else {
        //         this.$swal.fire({
        //             icon: 'error',
        //             title: 'Datos invalidos',
        //             text: this.errors[0]
        //         })
        //     }
        // },
        ...mapActions({
            setUser: 'setUser',
            setBalance: 'setBalance'
          }),
        async login(){
            this.errors = [];
            if (!this.credentials.email) {
                this.errors.push('El correo electrónico es requerido.');
            } else if (!this.validEmail(this.credentials.email)) {
                this.errors.push('El correo electrónico debe ser válido.');
            }
            if (!this.credentials.password) {
                this.errors.push("La contraseña es requerida");
            }
            
            if (this.errors.length === 0) {
                const auth = await login(this.credentials)
                if(auth){
                    this.setUser(auth)
                    this.setBalance(auth.balance)
                    this.$router.push('/dashboard')
                } else {
                    this.$swal.fire({
                        icon: 'error',
                        title: 'No Autorizado',
                        text: 'Sus credenciales son ivalidad'
                    })
                }
            } else {
                this.$swal.fire({
                    icon: 'error',
                    title: 'Datos invalidos',
                    text: this.errors[0]
                })
            }
        },
        validEmail: function (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
          }
    }
})
