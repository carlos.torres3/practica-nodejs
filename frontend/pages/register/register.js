import Vue from 'vue'
import { register } from '../../services/auth'


export default Vue.extend({
    name:'Register',
    head: {
        bodyAttrs: {
          class: 'text-center'
        },
        meta: [
            { name: 'viewport', content: 'width=device-width, initial-scale=1' }
        ]
    },
    layout: 'auth/auth',
    data:() =>({
        userInfo:{
            name: '',
            email:'',
            password:'',
            repassword:''
        },
        errors: []
    }),
    created(){
        
    },
    computed: {

    },
    mounted() {

    },
    methods:{
        async register(){
            this.errors = [];
            if (!this.userInfo.email) {
                this.errors.push('El correo electrónico es requerido.');
            } else if (!this.validEmail(this.userInfo.email)) {
                this.errors.push('El correo electrónico debe ser válido.');
            }
            if (!this.userInfo.password) {
                this.errors.push("La contraseña es requerida");
            }
            if (!this.userInfo.repassword) {
                this.errors.push("Debe confirmar la contraseña");
            }
            if (this.userInfo.password !== this.userInfo.repassword) {
                this.errors.push("Las contraseñas no coinciden");
            }
            
            if (this.errors.length === 0) {

                const reg = await register(this.userInfo)
                if(reg === true){
                    this.$router.push('/dashboard')
                } else {
                    this.$swal.fire({
                        icon: 'error',
                        title: 'Error en registro',
                        text: 'Se ha presentado un error en el registro, es posible que su email ya se encuentre registrado'
                    })
                }
            } else {
                this.$swal.fire({
                    icon: 'error',
                    title: 'Datos invalidos',
                    text: this.errors[0]
                })
            }
        },
        validEmail: function (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
          }
    }
})
