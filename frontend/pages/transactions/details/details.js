import Vue from 'vue'
import { getTransactionById } from '../../../services/transaction'

export default Vue.extend({
    name:'Dashboard',
    layout: 'dashboard/dashboard',
    data: ()=> ({
        transaction: {}
    }),
    async created(){
        await this.getTransactionById()
        console.log(this.transaction)
    },
    computed: {

    },
    mounted() {

    },
    methods:{
        async getTransactionById(){
            this.transaction= await getTransactionById(this.$route.params.id)
        }
    }
})
