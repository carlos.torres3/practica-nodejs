import Vue from 'vue'
import { depositTransaction } from '../../../services/transaction'
import { mapState } from 'vuex'

export default Vue.extend({
    name:'Dashboard',
    layout: 'dashboard/dashboard',
    data:() =>({
        transaction:{
            to:'',
            image:'',
            amount: 0
        },
        errors: []
    }),
    computed: {
        ...mapState({
            user: 'user'
        })
    },
    async created(){
        this.transaction.to = this.user.id
    },
    mounted() {

    },
    methods:{
        getFile(event){
            const reader = new FileReader()
            reader.onload = async (e) => {
                this.transaction.image = e.target.result
            }
            reader.readAsDataURL(event.target.files[0])
        },
        async deposit(){
            if(this.transaction.image !== ''){
                if(this.transaction.amount > 0){
                    const response = await depositTransaction(this.transaction)
                    if(response){
                        this.$swal.fire({
                            icon: 'info',
                            title: 'Depsito',
                            text: 'Deposito realizado con exito'
                        })
                        this.$router.push('/dashboard')
                    } else {
                        this.$swal.fire({
                            icon: 'error',
                            title: 'Deposito',
                            text: 'Error al realizar deposito'
                        })
                    }
                } else {
                    this.$swal.fire({
                        icon: 'error',
                        title: 'Deposito',
                        text: 'Debe colocar un monto de deposito mayor a 0'
                    })
                }
            } else {
                this.$swal.fire({
                    icon: 'error',
                    title: 'Deposito',
                    text: 'Debe cargar una imagen para el comprobante de deposito'
                })
            }
            
        }
        
    }
})
