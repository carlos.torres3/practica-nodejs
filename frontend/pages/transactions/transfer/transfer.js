import Vue from 'vue'
import { transferTransaction } from '../../../services/transaction'
import { mapState } from 'vuex'

export default Vue.extend({
    name:'Dashboard',
    layout: 'dashboard/dashboard',
    data:() =>({
        transaction:{
            to:'',
            amount: 0,
            from: ''
        },
        errors: []
    }),
    async created(){
        this.transaction.from = this.user.id
    },
    computed: {
        ...mapState({
            user: 'user'
        })
    },
    mounted() {

    },
    methods:{
        async transfer(){
            if(this.transaction.amount > 0){
                
                const response = await transferTransaction(this.transaction)
                if(response){
                    this.$swal.fire({
                        icon: 'info',
                        title: 'Transferencia',
                        text: 'Transferencia realizado con exito'
                    })
                    this.$router.push('/dashboard')
                } else {
                    this.$swal.fire({
                        icon: 'error',
                        title: 'Transferencia',
                        text: 'Error al realizar Transferencia'
                    })
                }
            } else {
                this.$swal.fire({
                    icon: 'error',
                    title: 'Transferencia',
                    text: 'Debe colocar un monto de Transferencia mayor a 0'
                })
            }            
        }
        
    }
})
