import Vue from 'vue'
import { withdrawTransaction } from '../../../services/transaction'
import { mapState } from 'vuex'

export default Vue.extend({
    name:'Withdraw',
    layout: 'dashboard/dashboard',
    data:() =>({
        transaction:{
            from:'',
            amount: 0
        },
        errors: []
    }),
    async created(){
        this.transaction.from = this.user.id
    },
    computed: {
        ...mapState({
            user: 'user'
        })
    },
    mounted() {

    },
    methods:{
        
        async withdraw(){
            if(this.transaction.amount > 0){
                const response = await withdrawTransaction(this.transaction)
                if(response){
                    this.$swal.fire({
                        icon: 'info',
                        title: 'Retiro',
                        text: 'Retiro realizado con exito'
                    }).then( ()=> {
                        this.$router.push('/dashboard')
                    })
                    
                } else {
                    this.$swal.fire({
                        icon: 'error',
                        title: 'Retiro',
                        text: 'Error al realizar Retiro'
                    })
                }
            } else {
                this.$swal.fire({
                    icon: 'error',
                    title: 'Retiro',
                    text: 'Debe colocar un monto de retiro mayor a 0'
                })
            }
            
        }
        
    }
})
