import Vue from 'vue'
import { getTransactions, confirmTransaction, cancelTransaction, getBalance } from '../../services/transaction'
import { mapActions, mapState } from 'vuex'

export default Vue.extend({
    name:'Dashboard',
    layout: 'dashboard/dashboard',
    data: ()=> ({
        transactions: [],
    }),
    created(){
        this.getTransactions()
    },
    computed: {
        ...mapState({
            user: 'user'
        })
    },
    mounted() {

    },
    methods:{
        ...mapActions({
            setBalance: 'setBalance'
          }),
        async getTransactions(){
            this.transactions= await getTransactions(this.user.id)
            this.setBalance = this.setBalance( await getBalance(this.user.id))
        },
        navigate(path, param=null){
            this.$router.push(path+param)
        },
        async confirm(transaction){
            if(transaction.type === 1){
                this.$swal.fire({
                    imageUrl: transaction.voucher,
                    imageHeight: 300,
                    imageAlt: 'Comprobante',
                    showCancelButton: true,
                    confirmButtonText: 'Si, confirmar deposito',
                    cancelButtonText: 'No, regresar!',
                    reverseButtons: true
                  }).then(async result => {
                      if(result.isConfirmed){
                          const respuesta = await confirmTransaction({
                              id: transaction.id,
                              type: transaction.type,
                              user: this.user.id,
                              image: null
                          })

                          if(respuesta){
                            this.$swal.fire({
                                icon: 'info',
                                title: 'Confirmar Deposito',
                                text: 'Deposito confirmado con exito'
                            }).then( ()=>{
                                this.getTransactions()
                            })
                        } else {
                            this.$swal.fire({
                                icon: 'error',
                                title: 'Confirmar Deposito',
                                text: 'Error al confirmar deposito'
                            }).then( ()=>{
                                this.getTransactions()
                            })
                        }
                      }
                  })
            } else {
                const { value: file } = await this.$swal.fire({
                title: 'Seleccione el comprobante',
                input: 'file',
                inputAttributes: {
                  'accept': 'image/*',
                  'aria-label': 'Seleccione el comprobante'
                }
              })
              
              if (file) {
                const reader = new FileReader()
                reader.onload = (e) => {
                  this.$swal.fire({
                    title: 'Verifique la información',
                    imageUrl: e.target.result,
                    imageHeight: 400,
                    imageAlt: 'Verifique la información',
                    showCancelButton: true,
                    confirmButtonText: 'Si, confirmar retiro',
                    cancelButtonText: 'No, regresar!',
                    reverseButtons: true
                  }).then( async result => {
                      if(result.isConfirmed){
                        const respuesta = await confirmTransaction({
                            id: transaction.id,
                            type: transaction.type,
                            user: this.user.id,
                            image: e.target.result
                        })

                        if(respuesta){
                            this.$swal.fire({
                                icon: 'info',
                                title: 'Confirmar Retiro',
                                text: 'Retiro confirmado con exito'
                            }).then( ()=>{
                                this.getTransactions()
                            })
                        } else {
                            this.$swal.fire({
                                icon: 'error',
                                title: 'Confirmar Retiro',
                                text: 'Error al confirmar retiro'
                            }).then( ()=>{
                                this.getTransactions()
                            })
                        }
                          
                      }
                  })
                }
                reader.readAsDataURL(file)
              }
            }
            
        },
        async cancel(transaction){
            this.$swal.fire({
                title: 'Desea cancelar la transacción',
                showDenyButton: true,
                confirmButtonText: `Si, Cancelar`,
                denyButtonText: `No, Regresar`,
              }).then(async (result) => {
                if (result.isConfirmed) {
                    if(result.isConfirmed){
                        const respuesta = await cancelTransaction({
                            id: transaction.id,
                            type: transaction.type,
                            user: this.user.id,
                            image: null
                        })

                        if(respuesta){
                          this.$swal.fire({
                              icon: 'info',
                              title: 'Cancelar transacción',
                              text: 'Transacción cancelada con exito'
                          }).then( ()=>{
                              this.getTransactions()
                          })
                      } else {
                          this.$swal.fire({
                              icon: 'error',
                              title: 'Cancelar transacción',
                              text: 'Error al cancelar transacción'
                          }).then( ()=>{
                              this.getTransactions()
                          })
                      }
                    }
                }
              })           
        },
        async createTransaction(type){
            switch (type) {
                case 1:
                  return this.$router.push('/transactions/deposit')
                case 2:
                    return this.$router.push('/transactions/withdraw')
                case 3:
                    return this.$router.push('/transactions/transfer')
              }
        }
        
    }
})
