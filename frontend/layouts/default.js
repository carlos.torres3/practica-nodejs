import Vue from 'vue'

export default Vue.extend({
    name:'Default',
    data: ()=>({
        user: null
    }),
    created(){
        this.getUser()
    },
    methods:{
        navigate(path){
            this.$router.push(path)
        },
        getUser(){
            this.user = localStorage.getItem('user')
        }
    }
})