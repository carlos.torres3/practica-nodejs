import Vue from 'vue'
import { logout } from '../../services/auth'
import { mapState } from 'vuex'

export default Vue.extend({
    name:'Dashboard',
    data: ()=>({
        
    }),
    computed: {
    ...mapState({
        user: 'user',
        balance: 'balance'
    }),
    },
    async created(){
        
    },
    methods:{
        navigate(path){
            this.$router.push(path)
        },
        logout(){
            let resp = logout()
            if(resp === true){
                this.$swal.fire({
                    icon: 'info',
                    title: 'Cierre de Sesión',
                    text: 'Su sesiónn ha sido cerrada con exito'
                })
                this.navigate('/')
            } else {
                this.$swal.fire({
                    icon: 'error',
                    title: 'Error en Servidor',
                    text: resp
                })
            }
        }
    }
})