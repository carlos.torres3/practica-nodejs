
export default function validateAuthRoute(context){
    if(localStorage.getItem('vuex') != undefined){
        if(context.route.path === '/login' ||  context.route.path === '/register') context.redirect('/dashboard')
        return
    } else {
        console.log(context.route.path)
        if(context.route.path !== '/login' && context.route.path !== '/' && context.route.path !== '/register') context.redirect('/login')
    }
}