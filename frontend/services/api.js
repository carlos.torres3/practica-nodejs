import axios from 'axios'

const urlApi = "http://192.168.1.100:3003/"
const headers = (token) => {
    return {
        headers: {
            'Content-Type': 'application/json',
            Authorization: token ? `Bearer ${token}` : ''
        }
    }
}

export const methodGet = async (endpoint, token = null) => {
    return await axios.get(urlApi + endpoint, headers(token))
} 

export const methodPost = async (endpoint, dataBody = {}, token = null) => {
    return await axios.post(urlApi + endpoint, dataBody, headers(token))
}