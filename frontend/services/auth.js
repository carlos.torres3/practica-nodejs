import { methodPost } from "./api"

// export const login = async (credentials)=> {
//     try {
//         const response = await methodPost('auth/login', credentials)
//         localStorage.setItem('token',response.data.data.token)
//         localStorage.setItem('user',JSON.stringify(response.data.data.user))
        
//         return true
//     } catch (error) {
//         return false
//     }
    
// }

export const login = async (credentials)=> {
    try {
        const response = await methodPost('auth/login', credentials)
        return response.data.data.user
    } catch (error) {
        return error
    }
    
}

export const register = async (userinfo)=> {
    try {
        const response = await methodPost('auth/register', userinfo)
        localStorage.setItem('token',response.data.data.token)
        localStorage.setItem('user',JSON.stringify(response.data.data.user))
        
        return true
    } catch (error) {
        return error
    }
}

export const logout = () => {
    try {
        localStorage.clear()
        return true
    } catch (error) {
        return error
    }
}


export const getByEmail = async (email)=> {
    try {
        const response = await methodPost('auth/getByEmail', {email: email})
        return response.data.data
    } catch (error) {
        return null
    }
}