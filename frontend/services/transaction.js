import { methodGet, methodPost } from "./api"
import { getByEmail } from '../services/auth'

export const getTransactions = async (userId)=> {
    try {
        const response = await methodGet(`transaction/user/${userId}`)
        return response.data.data
        
    } catch (error) {
        return null
    }
    
}

export const depositTransaction = async (transaction)=> {
    try {
        const responseImage = await methodPost('image', 
        { 
            type: 1,
            owner: 1,
            image: transaction.image 
        } )

    if(responseImage.data.data){
        const response = await methodPost(`transaction/deposit`, {
            to: transaction.to,
            amount: parseFloat(transaction.amount),
            voucher_id: responseImage.data.data.id
        } )
        return response.data.data
    }
        
    } catch (error) {
        return null
    }
    
}

export const withdrawTransaction = async (transaction)=> {
    try {
        const response = await methodPost(`transaction/withdraw`, {
            from: transaction.from,
            amount: parseFloat(transaction.amount)
        } )
        return response.data.data
        
    } catch (error) {
        return null
    }
    
}

export const transferTransaction = async (transaction)=> {
    
    try {
        const to = await getByEmail(transaction.to)
        const from = transaction.from
        const response = await methodPost(`transaction/transfer`, {
            from: from,
            to: to.id,
            amount: parseFloat(transaction.amount)
        } )
        return response.data.data
        
    } catch (error) {
        return null
    }
    
}

export const getBalance = async (userId)=> {
    try {
        const response = await methodGet(`transaction/balance/${userId}`)
        return response.data.data
    } catch (error) {
        return null
    }
    
}

export const getTransactionById = async (id)=> {
    try {
        const response = await methodGet(`transaction/id/${id}`)
        return response.data.data
    } catch (error) {
        return null
    }
    
}

export const confirmTransaction = async (transaction)=> {
    try {
        let responseImage = {}
        let responseTransaction = {}
        if(transaction.type === 1){
            const response = await methodPost('transaction/confirm', { id: transaction.id} )
            return response.data.data
        } else {
            responseImage = await methodPost('image', 
                { 
                    type: transaction.type,
                    owner: transaction.user,
                    image: transaction.image 
                } 
            )
            if(responseImage.data.data){
                responseTransaction = await methodPost('transaction/confirm', 
                    { 
                        id: transaction.id,
                        voucher_id: responseImage.data.data.id
                    } 
                )
            }
        return responseTransaction.data.data
        }
        
    } catch (error) {
        return null
    }
    
}

export const cancelTransaction = async (transaction)=> {
    try {
        const response = await methodPost('transaction/cancel', transaction )
        return response.data.data
        
    } catch (error) {
        return null
    }
    
}


