'use strict'

const TransactionService = require('../services/transactionService');

class TransactionController {
    constructor(){}

    async getByUserId(req, res) {
        const result =  await TransactionService.getByUserId(req.params.id)
        return res.status(result.statusCode).send(result)        
    }

    async getBalanceByUserId(req, res) {
        const result =  await TransactionService.getBalanceByUserId(req.params.id)
        return res.status(result.statusCode).send(result)        
    }

    async getById(req, res) {
        const result =  await TransactionService.getById(req.params.id)
        return res.status(result.statusCode).send(result)        
    }

    async confirm(req, res) {
        const result =  await TransactionService.confirm(req.body)
        return res.status(result.statusCode).send(result)        
    }

    async cancel(req, res) {
        const result =  await TransactionService.cancel(req.body)
        return res.status(result.statusCode).send(result)        
    }

    async deposit(req, res) {
        const result =  await TransactionService.deposit(req.body)
        return res.status(result.statusCode).send(result)        
    }

    async withdraw(req, res) {
        const result =  await TransactionService.withdraw(req.body)
        return res.status(result.statusCode).send(result)        
    }

    async transfer(req, res) {
        const result =  await TransactionService.transfer(req.body)
        return res.status(result.statusCode).send(result)        
    }

    async cancel(req, res) {
        const result =  await TransactionService.cancel(req.body)
        return res.status(result.statusCode).send(result)        
    }

}

module.exports = new TransactionController()