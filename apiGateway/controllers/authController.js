'use strict'

const AuthService = require('../services/authService');

class AuthController {
    constructor(){}
    
    async login(req, res){
        const result =  await AuthService.login(req.body)
        return res.status(result.statusCode).send(result) 
    }

    async register(req, res){
        const result =  await AuthService.register(req.body)
        return res.status(result.statusCode).send(result) 
    }

    async verify(req, res){
        const result =  await AuthService.verify(req.body.token)
        return res.status(result.statusCode).send(result) 
    }

    async getByEmail(req, res){
        const result =  await AuthService.getByEmail(req.body)
        return res.status(result.statusCode).send(result) 
    }

}
module.exports = new AuthController();