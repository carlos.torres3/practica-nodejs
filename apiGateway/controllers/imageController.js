
const ImageService = require('../services/imageService');

class ImageController {
    constructor(){}

    async create(req, res) {
        const result =  await ImageService.create(req.body)
        return res.status(result.statusCode).send(result)        
    }

}

module.exports = new ImageController()