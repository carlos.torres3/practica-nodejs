const axios = require('axios')

class Consul {
    constructor(){}

    async service(serviceName){
        try {
            const service = await axios.get(`http://192.168.1.100:8500/v1/agent/service/${serviceName}`)
            return {
                address: service.data.Address,
                port: service.data.Port
            }
            
        } catch (error) {
            return null
        }
        
    }
}

module.exports = new Consul()
                