
const { ApiResponse, ApiStatus } = require('../responses/apiResponse');
const Consul = require('../utils/consul')
const axios = require('axios');
const { PrismaClientRustPanicError } = require('@prisma/client/runtime');

class AuthService {
    
    constructor(){}

    async login(userCredentials){
        let balance
        try {
            const authService = await Consul.service(process.env.AUTH_SERVICE)
            const { data } = await axios.post(`http://${authService.address}:${authService.port}`+'/auth/login', userCredentials)
            if(data.data){
                const transactionService = await Consul.service(process.env.TRANSACTION_SERVICE)
                balance = await axios.get(`http://${transactionService.address}:${transactionService.port}/balance/${data.data.user.id}`)
            }
            data.data.user.balance = balance.data.data
            return new ApiResponse(ApiStatus.OK, 200,null, data.data) 
        } catch (error) {
            console.log(error)
            return new ApiResponse(ApiStatus.ERROR, 401,"No podemos autenticar con la información suministrada",null)
        }
        
    }

    async register(userInfo){
        try {
            let balance 
            const authService = await Consul.service(process.env.AUTH_SERVICE)
            const { data } = await axios.post(`http://${authService.address}:${authService.port}`+'/auth/register', userInfo)
            if(data.data){
                const transactionService = await Consul.service(process.env.TRANSACTION_SERVICE)
                balance = await axios.post(`http://${transactionService.address}:${transactionService.port}/balance/`,{ owner: data.data.user.id})
                
            }
            
            data.data.user.balance = balance.data.data
            return new ApiResponse(ApiStatus.OK, 201,null, data.data) 
        } catch (error) {
            return new ApiResponse(ApiStatus.ERROR, 400,"Error al registrar con la información suministrada",error)
        }
        
    }

    getById(id){
        return new Promise(async (resolve, reject)=>{
            try {
                const service = await Consul.service(process.env.AUTH_SERVICE)
                const { data } = await axios.get(`http://${service.address}:${service.port}/user/${id}`)
                resolve(data.data)
            } catch (error) {
                resolve(null)
            }
        })
    }

    async verify(token){
        try {
            const valid = jwt.verify(token, process.env.JWT_SECRET)
            return new ApiResponse(ApiStatus.OK, 200,"El token es valido",valid)             
        } catch (error) {
            return new ApiResponse(ApiStatus.ERROR, 401,"El token no es valido o ya expiro",null)
        }
    }

    async getByEmail(email){
        try {
            const authService = await Consul.service(process.env.AUTH_SERVICE)
            const { data } = await axios.post(`http://${authService.address}:${authService.port}`+'/user/getByEmail', email)
            return new ApiResponse(ApiStatus.OK, 200,null,data.data)             
        } catch (error) {
            return new ApiResponse(ApiStatus.ERROR, 400,error,null)
        }
    }

    
}

module.exports = new AuthService();