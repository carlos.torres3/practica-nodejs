
const { ApiResponse, ApiStatus } = require('../responses/apiResponse');
const Consul = require('../utils/consul')
const axios = require('axios');

class ImageService {
    
    constructor(){}

    getById(id){
        return new Promise(async (resolve, reject)=>{
            try {
                const service = await Consul.service(process.env.IMAGE_SERVICE)
                const { data } = await axios.get(`http://${service.address}:${service.port}/image/id/${id}`)
                resolve(data.data)
            } catch (error) {
                resolve(null)
            }
        })
    }

    async create(image){
        try {
            const service = await Consul.service(process.env.IMAGE_SERVICE)
            const { data } = await axios.post(`http://${service.address}:${service.port}/image`, image)
            return new ApiResponse(ApiStatus.OK, 200,null, data.data) 
        } catch (error) {
            return new ApiResponse(ApiStatus.ERROR, 200,error, null) 
        }
    }

}

module.exports = new ImageService();