const { ApiResponse, ApiStatus } = require('../responses/apiResponse');
const AuthService = require('./authService')
const ImageService = require('./imageService')
const Consul = require('../utils/consul')
const axios = require('axios')

class TransactionService {
    
    constructor(){}

    async getByUserId(id){
        try {
            const transactionService = await Consul.service(process.env.TRANSACTION_SERVICE)

            const { data } = await axios.get(`http://${transactionService.address}:${transactionService.port}/transaction/owner/${id}`)
            let result = []
            for(let x=0; x < data.data.length; x++){
                data.data[x].from = (await AuthService.getById(data.data[x].from)).name
                data.data[x].to = (await AuthService.getById(data.data[x].to)).name
                if(data.data[x].voucher_id) data.data[x].voucher = (await ImageService.getById(data.data[x].voucher_id)).image
                result.push(data.data[x])
            }
            
            return new ApiResponse(ApiStatus.OK, 200,null, data.data) 
        } catch (error) {
            console.log(error)
            return new ApiResponse(ApiStatus.ERROR, 200,error, null) 
        }
        
    }

    async getById(id){
        try {
            const transactionService = await Consul.service(process.env.TRANSACTION_SERVICE)

            const { data } = await axios.get(`http://${transactionService.address}:${transactionService.port}/transaction/id/${id}`)
            data.data.from = (await AuthService.getById(data.data.from)).name
                data.data.to = (await AuthService.getById(data.data.to)).name
                if(data.data.voucher_id) data.data.voucher = (await ImageService.getById(data.data.voucher_id)).image
            
            return new ApiResponse(ApiStatus.OK, 200,null, data.data) 
        } catch (error) {
            console.log(error)
            return new ApiResponse(ApiStatus.ERROR, 200,error, null) 
        }
        
    }

    async getBalanceByUserId(id){
        try {
            const transactionService = await Consul.service(process.env.TRANSACTION_SERVICE)

            const { data } = await axios.get(`http://${transactionService.address}:${transactionService.port}/balance/${id}`)
            
            
            return new ApiResponse(ApiStatus.OK, 200,null, data.data) 
        } catch (error) {
            console.log(error)
            return new ApiResponse(ApiStatus.ERROR, 200,error, null) 
        }
        
    }

    async confirm(transaction){
        try {
            const transactionService = await Consul.service(process.env.TRANSACTION_SERVICE)

            const { data } = await axios.post(`http://${transactionService.address}:${transactionService.port}/transaction/confirm`, transaction)
            console.log(data.data)
            if(data.data.status !== 0){
                return new ApiResponse(ApiStatus.OK, 200,'Transacción confirmada con exito', data.data) 
            }
            
        } catch (error) {
            return new ApiResponse(ApiStatus.ERROR, 400,error, null) 
        }
        
    }

    async deposit(transaction){
        try {
            const transactionService = await Consul.service(process.env.TRANSACTION_SERVICE)

            const { data } = await axios.post(`http://${transactionService.address}:${transactionService.port}/transaction/deposit`, transaction)
            console.log(data.data)
            return new ApiResponse(ApiStatus.OK, 200,'Deposito realizado con exito', data.data) 
            
        } catch (error) {
            return new ApiResponse(ApiStatus.ERROR, 400,error, null) 
        }
        
    }

    async withdraw(transaction){
        try {
            const transactionService = await Consul.service(process.env.TRANSACTION_SERVICE)

            const { data } = await axios.post(`http://${transactionService.address}:${transactionService.port}/transaction/withdraw`, transaction)
            return new ApiResponse(ApiStatus.OK, 200,'Retiro realizado con exito', data.data) 
            
        } catch (error) {
            return new ApiResponse(ApiStatus.ERROR, 400,error, null) 
        }
        
    }

    async transfer(transaction){
        try {
            const transactionService = await Consul.service(process.env.TRANSACTION_SERVICE)

            const { data } = await axios.post(`http://${transactionService.address}:${transactionService.port}/transaction/transfer`, transaction)
            return new ApiResponse(ApiStatus.OK, 200,'Transacción realizado con exito', data.data) 
            
        } catch (error) {
            return new ApiResponse(ApiStatus.ERROR, 400,error, null) 
        }
        
    }

    async cancel(transaction){
        try {
            const transactionService = await Consul.service(process.env.TRANSACTION_SERVICE)

            const { data } = await axios.post(`http://${transactionService.address}:${transactionService.port}/transaction/cancel`, transaction)
            return new ApiResponse(ApiStatus.OK, 200,'Transacción cancelada con exito', data.data) 
            
        } catch (error) {
            return new ApiResponse(ApiStatus.ERROR, 400,error, null) 
        }
        
    }


}

module.exports = new TransactionService()