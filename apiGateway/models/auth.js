const { PrismaClient } = require('@prisma/client')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt');
const prisma = new PrismaClient()

const fields = {
    id: true,
    name: true,
    email: true,
    password: false
}

class Auth {
    constructor(){}

    async login(userCredentials){
        const exist = await prisma.user.findUnique({
            where: {
                email: userCredentials.email,
            }
        })
        if(!exist){
            return null
        } else {
            return await bcrypt.compare(userCredentials.password,exist.password )
        }
    }
     
}

module.exports = new Auth()