const { PrismaClient } = require('@prisma/client')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt');
const prisma = new PrismaClient()

const fields = {
    id: true,
    name: true,
    email: true,
    password: false
}

class User {
    constructor(){}

    async getById(id){
        return await prisma.user.findUnique({
            where: {
                id: parseInt(id),
            },
            select: fields
        })
    } 

    async create(user){
        const exist = await prisma.user.findUnique({
            where: {
                email: user.email,
            }
        })
        if(exist) {
            return null
        } else {
            const password = await bcrypt.hash(user.password, 12);
            const result = await prisma.user.create({
                data: {
                    name: user.name,
                    email: user.email,
                    password: password
                },
                select: fields
            })
            return result
        }

    } 
}

module.exports = new User()