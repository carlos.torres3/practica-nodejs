const app = require('../app');
const supertest = require("supertest")(app);
const expect = require("chai").expect;

describe("Index", function () {
    it('Deberia retornar un status 200 - Index', async function () {
      const response = await supertest.get("/");
      expect(response.status).to.eql(200)
    });
    it('Deberia retornar un status 404 - Index', async function () {
      const response = await supertest.get("/123");
      expect(response.status).to.eql(404)
    });
    it('Deberia retornar un status 200 - Service Discovery', async function () {
      const response = await supertest.get("/health");
      expect(response.status).to.eql(200)
    });
  });