const app = require('../app');
const supertest = require("supertest")(app);
const expect = require("chai").expect;
const jwt = require('jsonwebtoken')

describe( "Auth", () => {
    it('Deberia retornar status code 401 - Login Fail - Wrong password', async function () {
        const response = await supertest
          .post("/auth/login")
          .send({
            email: "carlos.torres@pragma.com.co",
            password: "1234567"
          })
        expect(response.status).to.eql(401)
      });
    
      it('Deberia retornar status code 401 - Login Fail - Wrong email', async function () {
        const response = await supertest
          .post("/auth/login")
          .send({
            email: "carlos.torres100@pragma.com.co",
            password: "1234567"
          })
        expect(response.status).to.eql(401)
      });
    
      it('Deberia retornar status code 200 - Login Success', async function () {
        const response = await supertest
          .post("/auth/login")
          .send({
            email: "carlos.torres@pragma.com.co",
            password: "123456"
          })
        expect(response.status).to.eql(200)
        expect(response.body.data.token).to.exist
      });

      it('Deberia retornar status 200 - Token Verify Success', async function(){
        const payload = {
          check: true,
          exp: Math.floor(Date.now() / 1000) + (parseInt(process.env.JWT_TTL))
        }
        const token = jwt.sign(payload, process.env.JWT_SECRET)
        const response = await supertest
          .post("/auth/verify")
          .send({
            token: token
          })
        expect(response.status).to.eql(200)
        expect(response.body.data.check).to.exist
      });
      it('Deberia retornar status 401 - Token Verify Fail', async function(){
        const payload = {
          check: true,
          exp: Math.floor(Date.now() / 1000) + (parseInt(process.env.JWT_TTL))
        }
        const token = jwt.sign(payload, process.env.JWT_SECRET)
        const response = await supertest
          .post("/auth/verify")
          .send({
            token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"
          })
        expect(response.status).to.eql(401)
      });
});