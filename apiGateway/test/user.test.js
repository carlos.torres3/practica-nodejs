const app = require('../app');
const supertest = require("supertest")(app);
const expect = require("chai").expect;

describe("Users", function () {
  it('Deberia retornar un status 200 - getById Success', async function () {
    const response = await supertest.get("/user/1");
    expect(response.status).to.eql(200)
  });

  it('Deberia retornar un status 400 - GetById Fail', async function () {
    const response = await supertest.get("/user/10000");
    expect(response.status).to.eql(400)
  });

  it('Deberia retornar status code 201 - User Create Success- Email not exists', async function () {
    const response = await supertest
      .post("/user")
      .send({
        name: "Carlos Torres",
        email: "carlos.torres@pragma.com.co",
        password: "123456"
      })
    expect(response.status).to.eql(201)
  });

  it('Deberia retornar status code 400 - User Create Fail - Email exists', async function () {
    const response = await supertest
      .post("/user")
      .send({
        name: "Carlos Torres",
        email: "carlos.torres@pragma.com.co",
        password: "123456"
      })
    expect(response.status).to.eql(400)
  });
  
});





// it('Deberia retornar status code 201 - Crear usuario" ', () => {
//   supertest(app)
//   .post("/auth")
//   .set('Content-Type', 'application/json')
//   .set('Accept', 'application/json')
//   .send({
//     name: "Carlos Torres",
//     email: "carlos.torres@pragma.com.co",
//     password: "123456"
//   })
//   .expect(201)
//   .end()
// });

// it('Deberia retornar status code 400 - Email registrado" ', () => {
//   supertest(app)
//   .post("/auth")
//   .set('Content-Type', 'application/json')
//   .set('Accept', 'application/json')
//   .send({
//     name: "Carlos Torres",
//     email: "carlos.torres@pragma.com.co",
//     password: "123456"
//   })
//   .expect(400)
//   .end()
// });
// it('Deberia retornar status code 200 - Authenticacion', () => {
//    supertest(app)
//   .post("/auth/auth")
//   .set('Content-Type', 'application/json')
//   .set('Accept', 'application/json')
//   .send({
//     email: "carlos.torres@pragma.com.co",
//     password: "123456"
//   })
//   .expect(200)
//   .end()
// });
// it('Deberia retornar status code 401 - Clave Invalidad', () => {
//   supertest(app)
//  .post("/auth/auth")
//  .set('Content-Type', 'application/json')
//  .set('Accept', 'application/json')
//  .send({
//    email: "carlos.torres@pragma.com.co",
//    password: "1234567"
//  })
//  .expect(401)
//  .end()
// });
// it('Deberia retornar status code 401 - Email no existe', () => {
//   supertest(app)
//  .post("/auth/auth")
//  .set('Content-Type', 'application/json')
//  .set('Accept', 'application/json')
//  .send({
//    email: "carlos.torres12@pragma.com.co",
//    password: "1234567"
//  })
//  .expect(401)
//  .end()
// });
// it('Deberia retornar status code 401 - Token Invalido', () => {
//   supertest(app)
//   .post("/auth/verify")
//   .send({
//     token: "asd"
//   })
//   .expect(401)
// });

