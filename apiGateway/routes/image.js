const ImageController  = require('../controllers/imageController');
var express = require('express');
var router = express.Router();

router.post('/', async (req, res) => {
  return await ImageController.create(req, res)
});

module.exports = router;