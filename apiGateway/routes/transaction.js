const TransactionController  = require('../controllers/transactionController');
var express = require('express');
var router = express.Router();

router.get('/user/:id', async (req, res) => {
  return await TransactionController.getByUserId(req, res)
});

router.get('/balance/:id', async (req, res) => {
  return await TransactionController.getBalanceByUserId(req, res)
});

router.get('/id/:id', async (req, res) => {
  return await TransactionController.getById(req, res)
});

router.post('/confirm', async (req, res) => {
  return await TransactionController.confirm(req, res)
});

router.post('/cancel', async (req, res) => {
  return await TransactionController.cancel(req, res)
});

router.post('/deposit', async (req, res) => {
  return await TransactionController.deposit(req, res)
});

router.post('/withdraw', async (req, res) => {
  return await TransactionController.withdraw(req, res)
});

router.post('/transfer', async (req, res) => {
  return await TransactionController.transfer(req, res)
});

router.post('/cancel', async (req, res) => {
  return await TransactionController.cancel(req, res)
})

module.exports = router;