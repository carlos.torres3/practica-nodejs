'use strict'

const AuthController  = require('../controllers/authController');
var express = require('express');
var router = express.Router();

router.post('/login', async (req,res) => {
  return await AuthController.login(req,res)
});

router.post('/register', async (req,res) => {
  return await AuthController.register(req,res)
});

router.post('/verify', async (req,res) => {
  return await AuthController.verify(req,res)
});

router.post('/getByEmail', async (req,res) => {
  return await AuthController.getByEmail(req,res)
});

module.exports = router;
