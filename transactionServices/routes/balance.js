const BalanceController  = require('../controllers/balanceController');
var express = require('express');
var router = express.Router();

router.get('/:owner', async (req, res) => {
  return await BalanceController.getByOwner(req, res)
});

router.post('/', async (req, res) => {
  return await BalanceController.create(req, res)
});

module.exports = router;