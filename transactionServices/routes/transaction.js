const TransactionController  = require('../controllers/transactionController');
var express = require('express');
var router = express.Router();

router.get('/owner/:owner', async (req, res) => {
  return await TransactionController.getByOwner(req, res)
});

router.get('/id/:id', async (req, res) => {
  return await TransactionController.getById(req, res)
});

router.post('/deposit', async (req,res) => {
  return await TransactionController.deposit(req, res)
});

router.post('/confirm', async (req,res) => {
  return await TransactionController.confirm(req, res)
});

router.post('/cancel', async (req,res) => {
  return await TransactionController.cancel(req, res)
});

router.post('/withdraw', async (req,res) => {
  return await TransactionController.withdraw(req, res)
});

router.post('/transfer', async (req,res) => {
  return await TransactionController.transfer(req, res)
});


module.exports = router;