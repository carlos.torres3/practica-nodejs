const BalanceService = require('../services/balanceService');

class BalanceController {
    constructor(){}
    
    async getByOwner(req, res) {
        const result =  await BalanceService.getByOwner(req.params.owner)
        return res.status(result.statusCode).send(result)    
    }

    async create(req, res) {
        const result =  await BalanceService.create(req.body.owner)
        return res.status(result.statusCode).send(result)    
    }
    
}

module.exports = new BalanceController()