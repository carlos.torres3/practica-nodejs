const Transaction = require('../models/transaction');
const Balance = require('../models/balance');
const AuthService = require('../services/authService');
const ImageService = require('../services/imageService');
const { ApiResponse, ApiStatus } = require('../responses/apiResponse');


class TransactionService {
    
    constructor(){}

    
    async getByOwner(owner) {
        const result  = await Transaction.getByOwner(owner)
            if(result.length > 0){
                return new ApiResponse(ApiStatus.OK, 200,"Transacciones para : "+ owner,result)
            } else {
                return new ApiResponse(ApiStatus.OK, 200,"Sin transacciones para mostrar",null)
            }        
    }

    async getById(id) {
        const result  = await Transaction.getById(id)
            if(result){
                return new ApiResponse(ApiStatus.OK, 200,"Transacciones para : "+ id,result)
            } else {
                return new ApiResponse(ApiStatus.OK, 200,"Sin transacciones para mostrar",null)
            }        
    }

    async deposit(transaction){
        try {
            let balance
            let result
            const balanceFrom = await Balance.getByOwner(1)
            const balanceTo = await Balance.getByOwner(transaction.to)
            if(balanceFrom < transaction.amount){
                return new ApiResponse(ApiStatus.ERROR, 400,"Fondos insuficientes",null)
            } else {
                result = await Transaction.deposit(transaction)
                if(result){
                    balance = {
                        owner_id: balanceFrom.owner_id,
                        amount: parseFloat(balanceFrom.balance) - parseFloat(transaction.amount),
                        pending: parseFloat(balanceFrom.pending) + parseFloat(transaction.amount)
                    }
                    await Balance.set(balance)
                    balance = {
                        owner_id: balanceTo.owner_id,
                        pending: parseFloat(balanceTo.pending) + parseFloat(transaction.amount)
                    }
                    await Balance.set(balance)
                    return new ApiResponse(ApiStatus.OK, 201,"Transaccion guardada con exito",result)
                }
            }
            
            
            
        } catch (error) {
            return new ApiResponse(ApiStatus.ERROR, 400,"Se ha producido un error : " + error,null)
        }
    }

    async confirm(transaction){
        try {
            let result
            let balance
            const getTransaction = await Transaction.getById(transaction.id)
            if(getTransaction){

                if(transaction.voucher_id == undefined && getTransaction.type == 2){
                    return new ApiResponse(ApiStatus.ERROR, 400,"Parametro voucher_id es requerido para confirmar el retiro",null)
                }
                if(getTransaction.status != 0){
                    return new ApiResponse(ApiStatus.ERROR, 400,"La transaccion ya ha sido confirmada",null)
                }
                const balanceFrom = await Balance.getByOwner(getTransaction.from)
                const balanceTo = await Balance.getByOwner(getTransaction.to)
                
                transaction.type = getTransaction.type
                result = await Transaction.confirm(transaction)
                balance = {
                    owner_id: balanceFrom.owner_id,
                    pending: parseFloat(balanceFrom.pending) - parseFloat(getTransaction.amount)
                }
                await Balance.set(balance)
                balance = {
                    owner_id: balanceTo.owner_id,
                    amount: parseFloat(balanceTo.balance) + parseFloat(getTransaction.amount),
                    pending: parseFloat(balanceTo.pending) - parseFloat(getTransaction.amount)
                }
                await Balance.set(balance)
                
                return new ApiResponse(ApiStatus.OK, 200,"Transaccion confirmada con exito",result)
            } else {
                return new ApiResponse(ApiStatus.ERROR, 400,"Transaccion no existe",null)
            }            
        } catch (error) {
            return new ApiResponse(ApiStatus.ERROR, 400,"Se ha producido un error : " + error,null)
        }
    }

    async cancel(id){
        try {
            let result
            let balance
            const getTransaction = await Transaction.getById(id)
            if(getTransaction){
                if(getTransaction.status != 0){
                    return new ApiResponse(ApiStatus.ERROR, 400,"La transaccion no puede ser cancelada",null)
                }
                const balanceFrom = await Balance.getByOwner(getTransaction.from)
                const balanceTo = await Balance.getByOwner(getTransaction.to)
                
                result = await Transaction.cancel(getTransaction.id)
                balance = {
                    owner_id: balanceTo.owner_id,
                    pending: parseFloat(balanceFrom.pending) - parseFloat(getTransaction.amount)
                }
                await Balance.set(balance)
                balance = {
                    owner_id: balanceFrom.owner_id,
                    amount: parseFloat(balanceFrom.balance) + parseFloat(getTransaction.amount),
                    pending: parseFloat(balanceFrom.pending) - parseFloat(getTransaction.amount)
                }
                await Balance.set(balance)
                
                return new ApiResponse(ApiStatus.OK, 200,"Transaccion cancelada con exito",result)
            } else {
                return new ApiResponse(ApiStatus.ERROR, 400,"Transaccion no existe",null)
            }            
        } catch (error) {
            return new ApiResponse(ApiStatus.ERROR, 400,"Se ha producido un error : " + error,null)
        }
    }
    
    async withdraw(transaction){
        try {
            let balance
            let result
            const balanceFrom = await Balance.getByOwner(transaction.from)
            const balanceTo = await Balance.getByOwner(1)
            if(balanceFrom.balance < transaction.amount){
                return new ApiResponse(ApiStatus.ERROR, 400,"Fondos insuficientes",null)
            } else {
                result = await Transaction.withdraw(transaction)
                if(result){
                    balance = {
                        owner_id: balanceFrom.owner_id,
                        amount: parseFloat(balanceFrom.balance) - parseFloat(transaction.amount),
                        pending: parseFloat(balanceFrom.pending) + parseFloat(transaction.amount)
                    }
                    await Balance.set(balance)
                    balance = {
                        owner_id: balanceTo.owner_id,
                        pending: parseFloat(balanceTo.pending) + parseFloat(transaction.amount)
                    }
                    await Balance.set(balance)
                    return new ApiResponse(ApiStatus.OK, 201,"Transaccion guardada con exito",result)
                }
            }
            
            
            
        } catch (error) {
            return new ApiResponse(ApiStatus.ERROR, 400,"Se ha producido un error : " + error,null)
        }
    }

    async transfer(transaction){
        try {
            let balance
            let result
            const balanceFrom = await Balance.getByOwner(transaction.from)
            const balanceTo = await Balance.getByOwner(transaction.to)
            if(balanceFrom.balance < transaction.amount){
                return new ApiResponse(ApiStatus.ERROR, 400,"Fondos insuficientes",null)
            } else {
                result = await Transaction.transfer(transaction)
                if(result){
                    balance = {
                        owner_id: balanceFrom.owner_id,
                        amount: parseFloat(balanceFrom.balance) - parseFloat(transaction.amount)
                    }
                    await Balance.set(balance)
                    balance = {
                        owner_id: balanceTo.owner_id,
                        amount: parseFloat(balanceTo.balance) + parseFloat(transaction.amount)
                    }
                    await Balance.set(balance)
                    return new ApiResponse(ApiStatus.OK, 201,"Transaccion realizada con exito",result)
                }
            }
            
            
            
        } catch (error) {
            return new ApiResponse(ApiStatus.ERROR, 400,"Se ha producido un error : " + error,null)
        }
    }
    
}

module.exports = new TransactionService()

