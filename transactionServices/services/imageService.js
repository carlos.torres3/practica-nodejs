var consul = require('consul')(
    {
        host: process.env.HOST,
        secure:false
    }
);
const axios = require('axios').default;

class ImageService {
    
    constructor(){}

    async services() {
        return new Promise( (resolve, reject) => {
            consul.agent.service.list(async function(err, service) {
                if (err) {
                    resolve(err)
                };
                
                if(service[process.env.IMAGE_SERVICE_ID] === undefined) {
                    resolve(null)
                }else {
                    resolve(service[process.env.IMAGE_SERVICE_ID])
                }
                
                
            });
        });
    } 

    async getById(service,id){
        return new Promise( async (resolve, reject) => {
            try {
                const response = await axios.get("http://"+service.Address+":"+service.Port+"/image/id/"+id);
                if(response.status === 200){
                    resolve(response.data)
                } else {
                    resolve(null)
                }
              } catch (error) {
                resolve(null)
              }
        });
        
    }
}

module.exports = new ImageService()

