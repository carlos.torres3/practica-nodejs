const Balance = require('../models/balance');
const { ApiResponse, ApiStatus } = require('../responses/apiResponse');
const AuthService = require('../services/authService')

class BalanceService {
    
    constructor(){}

    async getByOwner(owner){
        const result  = await Balance.getByOwner(owner)
        console.log(result)
        if(result){
            return new ApiResponse(ApiStatus.OK, 200,null,result)
        } else {
            return new ApiResponse(ApiStatus.ERROR, 400,"Usuario no existe",null)
        }
    }

    async create(owner){
        try {
            const result = await Balance.create(owner)
            return new ApiResponse(ApiStatus.OK, 201,"Balance creado con exito",result)
        } catch (error) {
            return new ApiResponse(ApiStatus.ERROR, 400,"Se ha producido un error : " + error,null)
        }
    }
    
}

module.exports = new BalanceService()

