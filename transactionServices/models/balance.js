const { PrismaClient } = require('@prisma/client')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt');
const prisma = new PrismaClient()

class Balance {
    constructor(){}

    async getByOwner(owner){
        return await prisma.balance.findFirst({
            where: {
                owner_id: parseInt(owner)
            }
        })
    }

    async create(owner){
        const result = await prisma.balance.create({
            data: {
                owner_id: parseInt(owner)
            }
        })
        return result
    }

    async set(balance){
        const result = await prisma.balance.update({
            data: {
                balance: balance.amount,
                pending: balance.pending
            },
            where: {
                owner_id: balance.owner_id
            }
        })
        return result
    }     
}

module.exports = new Balance()