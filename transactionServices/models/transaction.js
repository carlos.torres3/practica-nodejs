const { PrismaClient } = require('@prisma/client')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt');
const balance = require('./balance');
const prisma = new PrismaClient()

class Transaction {
    constructor(){}

    

    async getByOwner(owner){
        return await prisma.transaction.findMany({
            
            where: {
                OR:[
                    {
                        from: parseInt(owner)
                    },
                    {
                        to: parseInt(owner)
                    }
                ]
            }
            
            
        })
    }

    async getById(id){
        return await prisma.transaction.findFirst({
            where: {
                id: parseInt(id)
            }
        })
    }

    async deposit(transaction){
        const result = await prisma.transaction.create({
            data: {
                from: 1,
                to: transaction.to,
                amount: transaction.amount,
                type: 1,
                voucher_id: transaction.voucher_id,
                description: "Deposito Pendiente",

            }
        })
        return result
    }

    async withdraw(transaction){
        const result = await prisma.transaction.create({
            data: {
                from: transaction.from,
                to: 1,
                amount: transaction.amount,
                type: 2,
                description: "Retiro Pendiente",

            }
        })
        return result
    }

    async confirm(transaction){
        let result
        if(transaction.type == 1){
            result = await prisma.transaction.update({
                data: {
                    status: 1,
                    description: "Deposito Completado"
                },
                where:{
                    id: parseInt(transaction.id)
                }
            })
        } else {
            result = await prisma.transaction.update({
                data: {
                    status: 1,
                    voucher_id: transaction.voucher_id,
                    description: "Retiro Completado"
                },
                where:{
                    id: parseInt(transaction.id)
                }
            })
        }
        
        return result
    }

    async cancel(id){
        const result = await prisma.transaction.update({
            data: {
                status: 2,
                description: "Transaccion Cancelada"
            },
            where:{
                id: parseInt(id)
            }
        })
        return result
    }

    async transfer(transaction){
        const result = await prisma.transaction.create({
            data: {
                from: transaction.from,
                to: transaction.to,
                amount: transaction.amount,
                type: 3,
                status: 1,
                description: "Transaccion Completada",

            }
        })
        return result
    }
     
}

module.exports = new Transaction()