/*
  Warnings:

  - You are about to alter the column `balance` on the `balance` table. The data in that column could be lost. The data in that column will be cast from `Decimal(65,30)` to `Decimal(10,2)`.
  - You are about to alter the column `pending` on the `balance` table. The data in that column could be lost. The data in that column will be cast from `Decimal(65,30)` to `Decimal(10,2)`.
  - You are about to alter the column `amount` on the `transaction` table. The data in that column could be lost. The data in that column will be cast from `Decimal(65,30)` to `Decimal(10,2)`.

*/
-- AlterTable
ALTER TABLE `balance` MODIFY `balance` DECIMAL(10, 2) NOT NULL DEFAULT 0,
    MODIFY `pending` DECIMAL(10, 2) NOT NULL DEFAULT 0;

-- AlterTable
ALTER TABLE `transaction` MODIFY `amount` DECIMAL(10, 2) NOT NULL;
