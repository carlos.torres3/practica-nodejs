class ApiResponse{
    constructor(status,statusCode,message,data){
            this.status = status
            this.statusCode = statusCode
            this.message = message
            this.data = data
    }
}

const ApiStatus = {
    OK: "OK",
    ERROR: "ERROR"
}


module.exports = { ApiResponse : ApiResponse, ApiStatus: ApiStatus }