const ImageController  = require('../controllers/imageController');
var express = require('express');
var router = express.Router();

router.get('/', async (req, res) => {
  return await ImageController.getAll(req, res)
});

router.get('/:owner', async (req, res) => {
  return await ImageController.getByOwner(req, res)
});

router.get('/id/:id', async (req, res) => {
  return await ImageController.getById(req, res)
});

router.post('/', async (req,res) => {
  return await ImageController.create(req, res)
});



module.exports = router;