/*
  Warnings:

  - You are about to drop the `imagen` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE `imagen`;

-- CreateTable
CREATE TABLE `Image` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `type` INTEGER NOT NULL DEFAULT 1,
    `owner` INTEGER NOT NULL,
    `image` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
