const app = require('../app');
const supertest = require("supertest")(app);
const expect = require("chai").expect;

describe("Images", function () {
  it('Deberia retornar un status 200 y data diferente de null - getByOwner Success', async function () {
    const response = await supertest.get("/image/1");
    expect(response.status).to.eql(200)
    expect(response.body.data).to.not.eql(null)
  });

  it('Deberia retornar un status 200 y data null - GetByOwner Fail', async function () {
    const response = await supertest.get("/image/10000");
    expect(response.status).to.eql(200)
    expect(response.body.data).to.eql(null)
  });

  it('Deberia retornar status code 201 - Image Create Success', async function () {
    const response = await supertest
      .post("/image")
      .send({
        type: 2,
        owner: 2,
        image: "IMAGEENBASE64EXAMPLE"
      })
    expect(response.status).to.eql(201)
  });

  it('Deberia retornar status code 400 - Image Create Fail - Wrong Type', async function () {
    const response = await supertest
      .post("/image")
      .send({
        type: "A",
        owner: 2,
        image: "IMAGEENBASE64EXAMPLE"
      })
    expect(response.status).to.eql(400)
  });

  it('Deberia retornar status code 400 - Image Create Fail - Wrong Owner', async function () {
    const response = await supertest
      .post("/image")
      .send({
        type: 1,
        owner: 100000,
        image: "IMAGEENBASE64EXAMPLE"
      })
    expect(response.status).to.eql(400)
  });
  
});
