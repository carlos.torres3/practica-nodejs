
const jwt = require('jsonwebtoken')
const Image = require('../models/image');
const { ApiResponse, ApiStatus } = require('../responses/apiResponse');
const AuthService = require('./authService')

class ImageService {
    
    constructor(){}

    async create(image){
        try {
            const service = await AuthService.services()
            if(service){
                const user = await AuthService.getUserById(service, image.owner)
                if(user){
                    const result = await Image.create(image)
                    return new ApiResponse(ApiStatus.OK, 201,"Imagen guardado con exito",result)
                } else {
                    return new ApiResponse(ApiStatus.ERROR, 400,"Owner no existe",null)
                }
            } else {
                return new ApiResponse(ApiStatus.ERROR, 500,"Error interno, El servicio de Autenticación no esta disponible",null)
            }
        } catch (error) {
            return new ApiResponse(ApiStatus.ERROR, 400,"Se ha producido un error : " + error,null)
        }
    }

    async getAll(){
        const result  = await Image.getAll()
        if(result.length > 0){
            return new ApiResponse(ApiStatus.OK, 201,null,result)
        } else {
            return new ApiResponse(ApiStatus.OK, 200,"Sin imagenes para mostrar",null)
        }
    }

    async getByOwner(owner) {
        const result  = await Image.getByOwner(owner)
            if(result.length > 0){
                return new ApiResponse(ApiStatus.OK, 200,"Imagenes para Owner : "+ owner,result)
            } else {
                return new ApiResponse(ApiStatus.OK, 200,"Sin imagenes para mostrar",null)
            }        
    }

    async getById(id) {
        const result  = await Image.getById(id)
            if(result){
                return new ApiResponse(ApiStatus.OK, 200,"Imagen para Id : "+ id,result)
            } else {
                return new ApiResponse(ApiStatus.OK, 400,"Sin imagen para mostrar",null)
            }        
    }
}

module.exports = new ImageService()

