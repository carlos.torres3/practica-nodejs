
const jwt = require('jsonwebtoken')
const Image = require('../models/image');
const { ApiResponse, ApiStatus } = require('../responses/apiResponse');
var consul = require('consul')(
    {
        host: process.env.HOST,
        secure:false
    }
);
const axios = require('axios').default;

class AuthService {
    constructor(){}

    async services() {
        return new Promise( (resolve, reject) => {
            consul.agent.service.list(async function(err, service) {
                if (err) {
                    resolve(err)
                };
                
                if(service[process.env.AUTH_SERVICE_ID] === undefined) {
                    resolve(null)
                }else {
                    resolve(service[process.env.AUTH_SERVICE_ID])
                }
                
                
            });
        });
    } 

    async getUserById(service,id){
        return new Promise( async (resolve, reject) => {
            try {
                const response = await axios.get("http://"+service.Address+":"+service.Port+"/user/"+id);
                if(response.status === 200){
                    resolve(response.data)
                } else {
                    resolve(null)
                }
              } catch (error) {
                resolve(null)
              }
        });
        
    }
            
}

module.exports = new AuthService()