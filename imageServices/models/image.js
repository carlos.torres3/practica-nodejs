const { PrismaClient } = require('@prisma/client')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt');
const prisma = new PrismaClient()

class Image {
    constructor(){}

    async create(image){
        const result = await prisma.image.create({
            data: {
                type: parseInt(image.type),
                owner: parseInt(image.owner),
                image: image.image
            }
        })
        return result
    }

    async getAll(){
        return await prisma.image.findMany()
    }

    async getByOwner(owner){
        return await prisma.image.findMany({
            where: {
                owner: parseInt(owner),
            }
        })
    } 

    async getById(id){
        return await prisma.image.findFirst({
            where: {
                id: parseInt(id),
            }
        })
    }

     
}

module.exports = new Image()