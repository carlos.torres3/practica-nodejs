const ImageService = require('../services/imageService');

class ImageController {
    constructor(){}

    async getByOwner(req, res) {
        const result =  await ImageService.getByOwner(req.params.owner)
        return res.status(result.statusCode).send(result)    
    }

    async getById(req, res) {
        const result =  await ImageService.getById(req.params.id)
        return res.status(result.statusCode).send(result)    
    }

    async create(req,res){
        const result =  await ImageService.create(req.body)
        return res.status(result.statusCode).send(result)
    }

    async getAll(req,res){
        const result =  await ImageService.getAll()
        return res.status(result.statusCode).send(result)
    }
}

module.exports = new ImageController()