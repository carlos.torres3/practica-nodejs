# Practica Ruta Node Js

# ![logo pragma](docs/images/logo.png)

En este proyecto practico se pretende demostrar los conocimientos y 
experiencia adquirida a lo largo de la Ruta de Aprendizaje Node Js.

Para esta practica se plantea el siguiente problema: 
> _Se requiere desarrollar una plataforma que permita a los usuarios registrados realizar transacciones
de dinero virtual entre ellos._

### **La aplicación debe permitir realizar las siguientes funcionalidades:**

+ Registro de usuarios nuevos, mediante correo electronico y contraseña.
+ Autenticar usuarios registrados, mediante correo elecrtronico y contraseña.
+ Cerrar sesión del usuario.
+ Visualizar el balance disponible y pendiente del usuario.
+ Visualizar el listado de las transacciones del usuario, con su información y estado.
+ Permitir realizar Depositos o Carga el balance.
+ Permitir realizar Retiros desde el balance.
+ Permitir realizar transferencia entre usuarios.
+ Permitir Confirmar o cancelar alguna transacción.

### **De debe tomar en cuenta los siguientes puntos en el desarrollo:**

+ Arquitectura de Microservicios.
+ Desarrollo Guiado por Pruebas (TDD).
+ Uso de contenedores (Docker).
+ Manejo de Imagenes en Base64.
+ Control de Versionamiento (GIT).
+ Autenticacion y protección de endpoint.

_Esta abierto a usar cualquier dependencia y libreria adicional necesaria_

# **Proceso de Desarrollo**

### **Esquema Propuesto**
&nbsp;
![Esquema Propuesto](docs/images/esquema.png)
&nbsp;

**Se realizan 3 proyectos en arquitectura de microservicios para manejar los diferentes modulos:**

+   Autenticación.
+   Imagenes.
+   Transacciones.
+   Cada microservicio maneja su base de datos independiente.

**Se realiza 1 proyecto Api rest para el manejo de las peticiones hacia los microservicios.**

**Se realiza 1 proyecto Frontend para el consumo del api y capa de presentacion al usuario final.**
&nbsp;

&nbsp;

### **Tecnologias Propuestas**
&nbsp;
![Tecnologias Propuestas](docs/images/tecnologias.png)
&nbsp;

Para los microservicios se utilizara tecnologia Javascript con el uso de NodeJs/Express, para la capa de presentación se usara
javascript con el uso de NuxtJs, para la persitencia de los dato usaremos MySql.

### **Flujos Propuestos**

&nbsp;

![Flujo Api](docs/images/flujoapi.PNG)

&nbsp;

![Flujo Mocroservicio](docs/images/flujomicroservicio.PNG)

&nbsp;

# **Funcionamiento**

&nbsp;

### Pantalla de bienvenida
![Pantalla de Bienvenida](docs/screenshots/bienvenida.PNG)

&nbsp;

### Registro
![Registro](docs/screenshots/registro.PNG)

&nbsp;

### Inicio de Sesión
![Inicio de Sesión](docs/screenshots/inicio.PNG)

&nbsp;

### Listado de Transacciones
![Listado de Transacciones](docs/screenshots/transacciones.PNG)

&nbsp;

### Deposito
![Deposito](docs/screenshots/deposito1.PNG)

&nbsp;

### Deposito Solicitado
![Deposito Solicitado](docs/screenshots/deposito2.PNG)

&nbsp;

### Confirmar deposito
![Confirmar deposito](docs/screenshots/deposito3.PNG)
![Confirmar deposito](docs/screenshots/deposito4.PNG)

&nbsp;

### Retiro
![Retiro](docs/screenshots/retiro1.PNG)

&nbsp;

### Retiro Solicitado
![Retiro Solicitado](docs/screenshots/retiro2.PNG)

&nbsp;

### Confirmar Retiro
![Confirmar Retiro](docs/screenshots/retiro3.PNG)
![Confirmar Retiro](docs/screenshots/retiro4.PNG)
![Confirmar Retiro](docs/screenshots/retiro5.PNG)
&nbsp;

### Transferir
![Transferir](docs/screenshots/transferir1.PNG)
![Transferir](docs/screenshots/transferir2.PNG)

&nbsp;

### Cerrar Sesión
![Cerrar Sesión](docs/screenshots/cerrar.PNG)

&nbsp;

# Implementación y Despliegue

&nbsp;

## Instalacion Consul (Service Discovery)


Este servicio permite registrar y monitorear los microservicios y es vital para el descubrimiento y acceso a los mismos

&nbsp;

+ Descargamos el ejecutable para nuestro sistema operativo https://www.consul.io/downloads y seguimos las intrucciones
+ Ejecutamos el servicio de Consul <code> c:\consul\consul.exe agent -dev -client=192.168.1.100 -data-dir=. </code>
+ Abrimos en navegador en la direccion <code> http://192.168.1.100:8500/ui/dc1/services </code>
![Consul UI](docs/screenshots/consul1.PNG)

&nbsp;

## Desplegar desde el codigo

&nbsp;

+ Clonar el proyecto desde https://gitlab.com/carlos.torres3/practica-nodejs.git

![Carpetas](docs/screenshots/folders.PNG)

+ **AuthenticationService**: Este microservicio es usado para el registro y Autenticacionde los usuarios.
+ **imageService**: Este microservicio es usado para el almacenamiento de las imagenes.
+ **transactionService**: Este microservicio es usado para manejar las transacciones.
+ **apiGateway**: Este servicio es el encargado de hacer la integración de los microservicios y exponer los endpoint necesarios.
+ **frontend**: Es la aplicación web que consume el apiGateway para realizar las operaciones.

&nbsp;

### Ejecutando el microservicio de Autenticación:

+ No dirigimos a la cartepa "authenticationService".
+ Renombrar el archivo <code>.env.example -> .env </code>
+ Colocar la cadena de conexión a la base de datos (MySQL) en la constante: <code> DATABASE_URL </code>
+ Ejecutamos: <code> npm install && npx prisma generate </code>

&nbsp;

### Ejecutando el microservicio de imagenes:

+ No dirigimos a la cartepa "imageService".
+ Renombrar el archivo <code>.env.example -> .env </code>
+ Colocar la cadena de conexión a la base de datos (MySQL) en la constante: <code> DATABASE_URL </code>
+ Ejecutamos: <code> npm install && npx prisma generate </code>
+ Ejecutamos: <code> npm start </code>

&nbsp;

### Ejecutando el microservicio de Transacciones:

+ No dirigimos a la cartepa "transactionService".
+ Renombrar el archivo <code>.env.example -> .env </code>
+ Colocar la cadena de conexión a la base de datos (MySQL) en la constante: <code> DATABASE_URL </code>
+ Ejecutamos: <code> npm install && npx prisma generate </code>
+ Ejecutamos: <code> npm start </code>

&nbsp;

### Ejecutando el Api Gateway:

+ No dirigimos a la cartepa "apiGateway".
+ Renombrar el archivo <code>.env.example -> .env </code>
+ Ejecutamos: <code> npm install </code>
+ Ejecutamos: <code> npm start </code>

### Ejecutando el Frontend:

+ No dirigimos a la cartepa "frontend".
+ Ejecutamos: <code> npm install </code>
+ Ejecutamos: <code> npm run build </code>
+ Ejecutamos: <code> npm run start </code>

&nbsp;

Verificamos que los servicios esten en linea:
![Consul UI](docs/screenshots/consul2.PNG)
Verificamos el Frontend: http://192.168.1.100:8000/
![Frontend](docs/screenshots/bienvenida.PNG)

&nbsp;

## Desplegar con Docker
&nbsp;

### Ejecutando el microservicio de Autenticación:

+ No dirigimos a la cartepa "authenticationService".
+ Renombrar el archivo <code>.env.example -> .env </code>
+ Colocar la cadena de conexión a la base de datos (MySQL) en la constante: <code> DATABASE_URL </code>
+ Ejecutamos: <code> npm install && npx prisma generate </code>
+ Ejecutamos: <code> docker build . -t yourdockerhubuser/authservice </code>
+ Ejecutamos: <code> docker run --rm -p 3000:3000 --name AuthService -d <yourdockerhubuser>/authservice </code>

&nbsp;

### Ejecutando el microservicio de imagenes:

+ No dirigimos a la cartepa "imageService".
+ Renombrar el archivo <code>.env.example -> .env </code>
+ Colocar la cadena de conexión a la base de datos (MySQL) en la constante: <code> DATABASE_URL </code>
+ Ejecutamos: <code> npm install && npx prisma generate </code>
+ Ejecutamos: <code> docker build . -t yourdockerhubuser/imageservice </code>
+ Ejecutamos: <code> docker run --rm -p 3001:3001 --name ImageService -d <yourdockerhubuser>/imageservice </code>

&nbsp;

### Ejecutando el microservicio de transacciones:

+ No dirigimos a la cartepa "transactionService".
+ Renombrar el archivo <code>.env.example -> .env </code>
+ Colocar la cadena de conexión a la base de datos (MySQL) en la constante: <code> DATABASE_URL </code>
+ Ejecutamos: <code> npm install && npx prisma generate </code>
+ Ejecutamos: <code> docker build . -t yourdockerhubuser/transactionservice </code>
+ Ejecutamos: <code> docker run --rm -p 3002:3002 --name TransactionService -d <yourdockerhubuser>/transactionservice </code>

&nbsp;

### Ejecutando el Api Gateway:

+ No dirigimos a la cartepa "transactionService".
+ Renombrar el archivo <code>.env.example -> .env </code>
+ Ejecutamos: <code> npm install </code>
+ Ejecutamos: <code> docker build . -t yourdockerhubuser/apigateway </code>
+ Ejecutamos: <code> docker run --rm -p 3003:3003 --name ApiGateway -d yourdockerhubuser/apigateway </code>

&nbsp;

### Ejecutando el Frontend:

+ No dirigimos a la cartepa "transactionService".
+ Renombrar el archivo <code>.env.example -> .env </code>
+ Ejecutamos: <code> npm install </code>
+ Ejecutamos: <code> docker build . -t yourdockerhubuser/apigateway </code>
+ Ejecutamos: <code> docker run --rm -p 8000:8000 --name Frontend -d yourdockerhubuser/frontend </code>

&nbsp;

### Carlos torres
### Auxiliar de Desarrollo
### carlos.torres@pragma.com.co
